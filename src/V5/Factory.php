<?php
namespace Axelmedia\Wordpress\Acf;

/**
 * プラグイン処理統括
 */
class Factory
{
    /**
     * 設定データ
     * 
     * @var Config
     */
    protected $config;

    protected $fields = array(
        Fields\JapanPostal::class,
        Fields\JapanPref::class,
    );

    /**
     * 静的呼び出し用関数
     * 
     * @param Config $config
     */
    public static function init(Config $config)
    {
        $self = new static($config);
        $self->enqueue();
        $self->register();
    }

    /**
     * コンストラクタ
     * 
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function enqueue()
    {
        // cleave.js
        // @see https://github.com/nosir/cleave.js
        wp_register_script(
            'cleavejs',
            $this->config->plugin_url.'assets/js/cleave.min.js',
            null,
            '1.1.2'
        );
        wp_enqueue_script('cleavejs');

        wp_register_script(
            $this->config->plugin_slug,
            $this->config->plugin_url.'assets/js/common.js',
            array('acf-input', 'acf-pro-input', 'cleavejs'),
            $this->config->current_version,
            true
        );
        wp_enqueue_script($this->config->plugin_slug);

        wp_register_style(
            $this->config->plugin_slug,
            $this->config->plugin_url.'assets/css/common.css',
            array('acf-input', 'acf-pro-input'),
            $this->config->current_version
        );
        wp_enqueue_style($this->config->plugin_slug);
    }

    public function register()
    {
        $settings = array(
            'version'	=> $this->config->current_version,
            'url'		=> $this->config->plugin_url,
            'path'		=> $this->config->plugin_path
        );

        foreach ($this->fields as $class) {
            new $class($this->config);
        }
    }
}
