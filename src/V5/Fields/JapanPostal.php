<?php
namespace Axelmedia\Wordpress\Acf\Fields;

/**
 * 郵便番号フィールド
 */
class JapanPostal extends AbstractField
{
    const FIELD_NAME = 'japan_postal';
    const FIELD_CATEGORY = 'basic';

    protected function boot()
    {
        $this->label = __('郵便番号', $this->config->textdomain);
    }

    protected function settings($field) {
        return array(
            array(
                'label'			=> __('値の取得方法', $this->config->textdomain),
                'type'			=> 'select',
                'name'			=> 'return_format',
                'choices'		=> array(
                    'full'			=> __('〒マーク＋ハイフンあり','acf'),
                    'has_hyphen'			=> __('ハイフンあり','acf'),
                    'no_hyphen'			=> __('ハイフンなし','acf'),
                )
            ),
        );
    }

    protected function render($field)
    {
        echo '<pre>';
        	print_r( $field );
        echo '</pre>';

        $atts = array();
        $keys = array( 'id', 'value', 'class', 'name', 'placeholder', 'maxlength', 'pattern' );
        $keys2 = array( 'readonly', 'disabled', 'required' );

        // atts (value="123")
        foreach( $keys as $k ) {
            if( isset($field[ $k ]) ) $atts[ $k ] = $field[ $k ];
        }
        
        // atts2 (disabled="disabled")
        foreach( $keys2 as $k ) {
            if( !empty($field[ $k ]) ) $atts[ $k ] = $k;
        }
        
        $atts['type'] = 'text';
        $atts['class'] = (!empty($atts['class']) ? $atts['class'].' acf-'.$field['type'] : 'acf-'.$field['type']);

        // remove empty atts
        $atts = acf_clean_atts( $atts );

        $html = '';

        $html .= '<div class="acf-input-prepend">〒</div>';
        $html .= '<div class="acf-input-wrap">' . acf_get_text_input( $atts ) . '</div>';

        echo $html;
    }

    public function format_value($value, $post_id, $field)
    {
        if (!empty($value)) {
            $format = $field['return_format'];
            $value = preg_replace('/[^\d]+/isu', '', $value);

            if ('full' === $format || 'has-hyphen' === $format) {
                $value = preg_replace('/\A(\d{3})(\d{4})/isu', '$1-$2', $value);

                if ('full' === $format) {
                    $value = '〒'.$value;
                }
            }
        }

        return $value;
    }
}
