<?php
namespace Axelmedia\Wordpress\Acf\Fields;

/**
 * 郵便番号フィールド
 */
class JapanPref extends AbstractField
{
    /**
     * フィールド名（ID）
     */
    const FIELD_NAME = 'japan_pref';

    /**
     * フィールドタイプ
     */
    const FIELD_CATEGORY = 'choice';

    /**
     * 初期起動（コンストラクタ）
     */
    protected function boot()
    {
        $this->label = __('都道府県', $this->config->textdomain);
    }

    /**
     * デフォルト設定
     */
    protected function defaults()
    {
        return array(
            'multiple'      =>	0,
            'allow_null'    =>	0,
            'choices'       =>	array(),
            'default_value' => '',
            'ui'            => 1,
            'return_format' => 'array',
            'placeholder'   => __('Select a prefecture', 'acf-japan-pref'),
        );
    }

    /**
     * フィールド設定項目
     */
    protected function settings($field) {
        return array(
            array(
                'label'			=> __('Choices', 'acf'),
                'name'			=> 'choices',
                'type'			=> 'textarea',
                'wrapper' => array(
                    'class' => 'hidden',
                ),
                'value' => acf_encode_choices($prefs),
            ),

            // 空の許可
            array(
                'label'			=> __('Allow Null?', 'acf'),
                'instructions'	=> '',
                'name'			=> 'allow_null',
                'type'			=> 'true_false',
                'ui'			=> 1,
            ),

            // 複数選択許可
            array(
                'label'			=> __('Select multiple values?', 'acf'),
                'instructions'	=> '',
                'name'			=> 'multiple',
                'type'			=> 'true_false',
                'ui'			=> 1,
            ),

            // スタイリッシュなUI
            array(
                'label'			=> __('Stylised UI', 'acf'),
                'instructions'	=> '',
                'name'			=> 'ui',
                'type'			=> 'true_false',
                'ui'			=> 1,
            ),

            // return_format
            array(
                'label'			=> __('Return Format','acf'),
                'instructions'	=> __('Specify the value returned','acf'),
                'type'			=> 'select',
                'name'			=> 'return_format',
                'choices'		=> array(
                    'value'			=> __('Value','acf'),
                    'label'			=> __('Label','acf'),
                    'array'			=> __('Both (Array)','acf')
                )
            ),
        );
    }

    protected function render($field)
    {
        // echo '<pre>';
        // 	print_r( $field );
        // echo '</pre>';

        // convert
        $value = acf_get_array($field['value']);
        // $choices = acf_get_array($field['choices']);
		$choices = self::get_prefs();
		
		
		// placeholder
		if( empty($field['placeholder']) ) {
			$field['placeholder'] = _x('Select', 'verb', 'acf');
		}
		
		
		// add empty value (allows '' to be selected)
		if( empty($value) ) {
			$value = array('');
		}
		
		
		// allow null
		// - have tried array_merge but this causes keys to re-index if is numeric (post ID's)
		if( $field['allow_null'] && !$field['multiple'] ) {
			
			$prepend = array(''	=> '- ' . $field['placeholder'] . ' -');
			$choices = $prepend + $choices;
			
		}
		
		
		// vars
		$select = array(
			'id'				=> $field['id'],
			'class'				=> $field['class'],
			'name'				=> $field['name'],
			'data-ui'			=> $field['ui'],
			'data-ajax'			=> $field['ajax'],
			'data-multiple'		=> $field['multiple'],
			'data-placeholder'	=> $field['placeholder'],
			'data-allow_null'	=> $field['allow_null']
        );
        
        // multiple
		if( $field['multiple'] ) {
		
			$select['multiple'] = 'multiple';
			$select['size'] = 5;
			$select['name'] .= '[]';
			
		}
		
		
		// special atts
		if( !empty($field['readonly']) ) $select['readonly'] = 'readonly';
		if( !empty($field['disabled']) ) $select['disabled'] = 'disabled';
		if( !empty($field['ajax_action']) ) $select['data-ajax_action'] = $field['ajax_action'];

        // hidden input
		if( $field['ui'] ) {
			
			$v = $value;
			
			if( $field['multiple'] ) {
				
				$v = implode('||', $v);
				
			} else {
				
				$v = acf_maybe_get($v, 0, '');
				
			}
			
			acf_hidden_input(array(
				'id'	=> $field['id'] . '-input',
				'name'	=> $field['name'],
				'value'	=> $v
			));
			
		} elseif( $field['multiple'] ) {
			
			acf_hidden_input(array(
				'id'	=> $field['id'] . '-input',
				'name'	=> $field['name']
			));
			
		}

        // append
		$select['value'] = $value;
		$select['choices'] = $choices;
		
		
		// render
		acf_select_input( $select );
    }

    function input_admin_enqueue_scripts() {
		
		// bail ealry if no enqueue
	   	if( !acf_get_setting('enqueue_select2') ) return;
	   	
	   	
		// globals
		global $wp_scripts, $wp_styles;
		
		
		// vars
		$min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
		$major = acf_get_setting('select2_version');
		$version = '';
		$script = '';
		$style = '';
		
		
		// attempt to find 3rd party Select2 version
		// - avoid including v3 CSS when v4 JS is already enququed
		if( isset($wp_scripts->registered['select2']) ) {
			
			$major = (int) $wp_scripts->registered['select2']->ver;
		
		}
		
		
		// v4
		if( $major == 4 ) {
			
			$version = '4.0';
			$script = acf_get_url("assets/inc/select2/4/select2.full{$min}.js");
			$style = acf_get_url("assets/inc/select2/4/select2{$min}.css");
		
		// v3
		} else {
			
			$version = '3.5.2';
			$script = acf_get_url("assets/inc/select2/3/select2{$min}.js");
			$style = acf_get_url("assets/inc/select2/3/select2.css");
			
		}
		
		
		// enqueue
		wp_enqueue_script('select2', $script, array('jquery'), $version );
		wp_enqueue_style('select2', $style, '', $version );
		
	}

    public function format_value($value, $post_id, $field)
    {
        // if (!empty($value)) {
        //     $format = $field['return_format'];
        //     $value = preg_replace('/[^\d]+/isu', '', $value);

        //     if ('full' === $format || 'has-hyphen' === $format) {
        //         $value = preg_replace('/\A(\d{3})(\d{4})/isu', '$1-$2', $value);

        //         if ('full' === $format) {
        //             $value = '〒'.$value;
        //         }
        //     }
        // }

        return $value;
    }

    public function get_prefs()
    {
        $pref_ids = array(
            '01' => '北海道', '02' => '青森県', '03' => '岩手県', '04' => '宮城県', '05' => '秋田県',
            '06' => '山形県', '07' => '福島県', '08' => '茨城県', '09' => '栃木県', '10' => '群馬県',
            '11' => '埼玉県', '12' => '千葉県', '13' => '東京都', '14' => '神奈川県', '15' => '新潟県',
            '16' => '富山県', '17' => '石川県', '18' => '福井県', '19' => '山梨県', '20' => '長野県',
            '21' => '岐阜県', '22' => '静岡県', '23' => '愛知県', '24' => '三重県', '25' => '滋賀県',
            '26' => '京都府', '27' => '大阪府', '28' => '兵庫県', '29' => '奈良県', '30' => '和歌山県',
            '31' => '鳥取県', '32' => '島根県', '33' => '岡山県', '34' => '広島県', '35' => '山口県',
            '36' => '徳島県', '37' => '香川県', '38' => '愛媛県', '39' => '高知県', '40' => '福岡県',
            '41' => '佐賀県', '42' => '長崎県', '43' => '熊本県', '44' => '大分県', '45' => '宮崎県',
            '46' => '鹿児島県', '47' => '沖縄県',
        );
        return $pref_ids;
    }
}
