<?php
namespace Axelmedia\Wordpress\Acf;

/**
 * 設定データ
 * 
 * @see http://php.net/manual/ja/class.arrayobject.php
 */
class Config extends \ArrayObject
{
    /**
     * コンストラクタ
     * 
     * @param mixed $input
     */
    public function __construct($input)
    {
        parent::__construct(
            $input,
            static::STD_PROP_LIST | static::ARRAY_AS_PROPS
        );
    }
}
