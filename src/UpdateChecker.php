<?php
namespace Axelmedia\Wordpress\Acf;

/**
 * アップデートチェッカー
 */
class UpdateChecker
{
    /**
     * 設定データ
     * 
     * @var Config
     */
    protected $config;

    /**
     * リポジトリパス
     * 
     * @var string
     */
    protected $repo = 'axelmedia';

    /**
     * ブランチ名
     * 
     * @var string
     */
    protected $branch = 'master';

    /**
     * 更新データ情報のファイル名
     * 
     * @var string
     */
    protected $infoFile = 'info.json';

    /**
     * 更新チェック間隔
     * 
     * @var int
     */
    // protected $cacheSec = 43200;// 12 hours
    protected $cacheSec = 0;

    /**
     * 更新チェックデータ用の名前
     * 
     * @var string
     */
    protected $transientName;

    /**
     * 初期処理
     * 
     * @param Config
     */
    public static function init(Config $config)
    {
        $self = new static($config);
        add_filter('plugins_api', array($self, 'pluginInformation'), 20, 3);
        add_filter('site_transient_update_plugins', array($self, 'siteTransient'), 20, 3);
        add_filter('upgrader_package_options', array($self, 'upgraderOptions'), 20, 3);
        add_action('upgrader_process_complete', array($self, 'processComplete'), 10, 2);
    }

    /**
     * コンストラクタ
     * 
     * @param Config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;

        // 更新チェックデータ用の名前
        $this->transientName = $config->plugin_slug.'_updator';
    }

    /**
     * 更新情報の表示用データ取得
     * @see https://developer.wordpress.org/reference/hooks/plugins_api/
     * 
     * @param mixed $result
     * @param string $action
     * @param object $args
     * @return mixed
     */
    public function pluginInformation($result, $action, $args)
    {
        // 対象の処理でなければ無視
        if (
            'plugin_information' !== $action
            || $this->config->plugin_slug !== $args->slug
        ) {
            return false;
        }

        if ($response = $this->getRemote()) {
            $remote = $response->content;
            $lastModified = new \DateTime($response->headers['last-modified']);

            $data = array(
                'name' => $remote->name,
                'slug' => $this->config->plugin_slug,
                'version' => $remote->version,
                'tested' => $remote->tested,
                'requires' => $remote->requires,
                'download_link' => $remote->download_url,
                'trunk' => $remote->download_url,
                'last_updated' => $lastModified->format('Y-m-d H:i:s'),
                'sections' => new \stdClass(),
            );

            if (isset($remote->sections->description)) {
                $data['sections']->description = $remote->sections->description;
            }

            if (isset($remote->sections->installation)) {
                $data['sections']->installation = $remote->sections->installation;
            }

            if (isset($remote->sections->changelog)) {
                $data['sections']->changelog = $remote->sections->changelog;
            }

            if (!empty($remote->sections->screenshots)) {
                $data['sections']->screenshots = $remote->sections->screenshots;
            }

            if (!empty($remote->banners)) {
                $data['banners'] = $remote->banners;
            }

            return (object) $data;
        }

        return false;
    }

    /**
     * 更新有無のチェック
     * @see http://hookr.io/filters/site_transient_update_plugins/
     * 
     * @param array $transient
     * @return array
     */
    public function siteTransient($transient)
    {
        if (empty($transient->checked)) {
            return $transient;
        }
 
        if ($response = $this->getRemote()) {
            $remote = $response->content;

            if(
                version_compare($this->config->current_version, $remote->version, '<')
                && version_compare($remote->requires, get_bloginfo('version'), '<')
            ) {
                $data = array(
                    'package' => $remote->download_url,
                    'slug' => $this->config->plugin_slug,
                    'plugin' => $this->config->plugin_file,
                    'new_version' => $remote->version,
                    'tested' => $remote->tested,
                    'url' => $remote->homepage,
                    'compatibility' => new \stdClass(),
                );

                $transient->response[$res->plugin] = (object) $data;
            }
        }

        return $transient;
    }

    /**
     * アップデート処理時のパラメータ更新
     * @see https://developer.wordpress.org/reference/hooks/upgrader_package_options/
     * 
     * @param array $options
     * @return array
     */
    public function upgraderOptions($options)
    {
        // zipファイル解凍先を指定
        $options['destination'] = $this->config->plugin_path;
        return $options;
    }

    /**
     * アップデート完了後に更新チェックデータを削除
     * @see https://developer.wordpress.org/reference/hooks/upgrader_process_complete/
     * 
     * @param WP_Upgrader $upgrader
     * @param array $hook_extra
     */
    public function processComplete($upgrader, $hook_extra)
    {
        if ('update' === $hook_extra['action'] && 'plugin' === $hook_extra['type']) {
            delete_transient($this->trasientName);
        }
    }

    /**
     * リポジトリから更新情報を取得
     * 
     * @return mixed
     */
    protected function getRemote()
    {
        $url = 'https://bitbucket.org';
        $url .= '/'.$this->repo;
        $url .= '/'.$this->config->plugin_slug;
        $url .= '/raw/'.$this->branch;
        $url .= '/'.$this->infoFile;

        // info.json is the file with the actual plugin information on your server
        $remote = wp_remote_get($url, array(
            'timeout' => 10
        ));

        if (
            !is_wp_error($remote)
            && isset($remote['response']['code'])
            && 200 === $remote['response']['code']
            && !empty($remote['body'])
        ) {
            set_transient($this->transientName, $remote, $this->cacheSec);
            return (object) array(
                'headers' => $remote['headers']->getAll(),
                'content' => json_decode($remote['body']),
            );
        }
    }
}
