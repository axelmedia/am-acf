<?php

/**
 * Plugin Name: AxelMedia ACF Pack
 * Plugin URI: 
 * Description: AxelMedia Advanced Custom Fields Pack
 * Version: 0.1.2
 * Author: AxelMedia
 * Author URI: https://axel-media.com
 * Copyright: AxelMedia
 */

// ダイレクトアクセス防止
if (!defined('ABSPATH')) exit;

// 関数が重複していなければ処理開始
if (!function_exists('__am_acf_register')) {
    function __am_acf_register ($version = false) {
        // ACFのバージョン
        if (!$version || 5 != $version) {
            $version = 4;
        }

        // autoload設定
        spl_autoload_register (function ($className) use ($version) {
            $namespace = 'Axelmedia\Wordpress\Acf';
            if (0 === strpos($className, $namespace)) {
                $root = __DIR__.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR;
                $path = substr($className, strlen($namespace) + 1);
                $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
                $file = $root.'V'.$version.DIRECTORY_SEPARATOR.$path.'.php';
                if (is_file($file)) {
                    include_once($file);
                } else {
                    $file = $root.$path.'.php';
                    if (is_file($file)) {
                        include_once($file);
                    }
                }
            }
        });

        // 現プラグイン設定を取得
        $json = json_decode(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'info.json'));

        // 設定データ作成
        $config = new Axelmedia\Wordpress\Acf\Config(array(
            'plugin_slug' => $json->slug,
            'plugin_url' => plugin_dir_url(__FILE__),
            'plugin_path' => plugin_dir_path(__FILE__),
            'plugin_basename' => plugin_basename(__FILE__),
            'plugin_file' => basename(__DIR__).'/'.basename(__FILE__),
            'current_version' => $json->version,
            'textdomain' => $json->slug,
        ));

        // アップデートチェッカー起動
        Axelmedia\Wordpress\Acf\UpdateChecker::init($config);

        // フィールド処理起動
        Axelmedia\Wordpress\Acf\Factory::init($config);
    }

    // プラグインをフック
    add_action('acf/include_field_types', '__am_acf_register'); // v5
    add_action('acf/register_fields', '__am_acf_register'); // v4
}
